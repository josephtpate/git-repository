
%% Question 1
v=-1;p=1;
q = (exp(v-p))/(1+exp(v-p)+exp(v-p)+exp(v-p))
% market share of the inside goods is a vector
q_0 = 1/(1+exp(v-p)+exp(v-p)+exp(v-p))

%% Question 1 Answer: For q = qa = qb = qc
% q =

%     0.0963


% q_0 =

%     0.7112
%%
%% Question 2   
addpath('CEtools');
optset('broyden','showiters',1000);
optset('broyden','maxit',300) ;
optset('broyden','tol',1e-8) ;
% this is sloppy stopping rule.

f = @(P) [exp(-1-P(1))/(1+exp(-1-P(1))+exp(-1-P(2))+exp(-1-P(3))) - P(1)*exp(-1-P(1))/(1+exp(-1-P(1))+exp(-1-P(2))+exp(-1-P(3)))*(1-exp(-1-P(1))/(1+exp(-1-P(1))+exp(-1-P(2))+exp(-1-P(3))));
          exp(-1-P(2))/(1+exp(-1-P(1))+exp(-1-P(2))+exp(-1-P(3))) - P(2)*exp(-1-P(2))/(1+exp(-1-P(1))+exp(-1-P(2))+exp(-1-P(3)))*(1-exp(-1-P(2))/(1+exp(-1-P(1))+exp(-1-P(2))+exp(-1-P(3))));
          exp(-1-P(3))/(1+exp(-1-P(1))+exp(-1-P(2))+exp(-1-P(3))) - P(3)*exp(-1-P(3))/(1+exp(-1-P(1))+exp(-1-P(2))+exp(-1-P(3)))*(1-exp(-1-P(3))/(1+exp(-1-P(1))+exp(-1-P(2))+exp(-1-P(3))))];

%% Question 2: Starting values: [1,1,1]
tic
a=1;
b=1;
c=1;
P = broyden(f,[a;b;c])
% you could also have extracted number of iterations form broyden run like
% [p,~,~,nit] = broyden(...)
f(P)
toc
%Answer
% P =
%     1.0985
%     1.0985
%     1.0985

% f(P) =
%    1.0e-10 *
%     0.3547
%     0.3547
%     0.3547
% Elapsed time is 0.006984 seconds.
%% Question 2: Starting values: [0,0,0]
tic
a=0;
b=0;
c=0;
P = broyden(f,[a;b;c])
f(P)
toc
%Answer
% P =
%     1.0985
%     1.0985
%     1.0985

% f(P) =
%    1.0e-08 *
%     0.3623
%     0.3623
%     0.3623
% Elapsed time is 0.006950 seconds.
%% Question 2: Starting values: [0,1,2]
tic
a=0;
b=1;
c=2;
P = broyden(f,[a;b;c])
f(P)
toc
%Answer
% P =
%    1.0e+08 *
%     0.5034
%    -0.0857
%     1.4820

% f(P) =
%      0
%    NaN
%      0
% Elapsed time is 0.007736 seconds.
%% Question 2: Starting values: [3,2,1]
tic
a=3;
b=2;
c=1;
P = broyden(f,[a;b;c])
f(P)
toc
%Answer
% P =
%    22.5832
%     1.1083
%     1.1083

% f(P) =
%    1.0e-08 *
%    -0.0995
%    -0.1372
%    -0.0004
% Elapsed time is 0.009277 seconds.
%%
%% Question 4
tol = 1e-8;


%% Question 4: Starting values: [1,1,1]
tic

a=1;
b=1;
c=1;
it=1;

P=[a,b,c];
Pn = [0,0,0];
while abs(P-Pn)>tol
Q=exp(-1-P)/(1+exp(-1-P(1))+exp(-1-P(2))+exp(-1-P(3)));
Pn=1./(1.-Q);
P=Pn;
it = it+1;
if it>=100
    break
end  
end
it
P
Q
toc
%Answer
% it =
%      2
% P =
%     1.1065    1.1065    1.1065
% Q =
%     0.0963    0.0963    0.0963
% Elapsed time is 0.008762 seconds.
%% Question 4: Starting values: [0,0,0]
tic

a=0;
b=0;
c=0;
it=1;

P=[a,b,c];
Pn = [0,0,0];
% your algorithm does not do any step at all since final Pn equals the
% starting one. be careful with your algorithm making at least one step
while abs(P-Pn)>tol
P=Pn;
Q=exp(-1-P)/(1+exp(-1-P(1))+exp(-1-P(2))+exp(-1-P(3)));
Pn=1./(1.-Q);
it = it+1;
if it>=100
    break
end  
end
it
Pn
Q
toc
% this iterations should also haev converged. 
%Answer
% it =
%      1
% Pn =
%      0     0     0
% Q =
%     0.0963    0.0963    0.0963
% Elapsed time is 0.011300 seconds.
%% Question 4: Starting values: [0,1,2]
tic

a=0;
b=1;
c=2;
it=1;

P=[a,b,c];
Pn = [0,0,0];
while abs(P-Pn)>tol % this consiftion should be max(abs(P-Pn)) otherwise it makes no sence
P=Pn;
Q=exp(-1-P)/(1+exp(-1-P(1))+exp(-1-P(2))+exp(-1-P(3)));
Pn=1./(1.-Q);
it = it+1;
if it>=100
    break
end  
end
it
Pn
Q
% again, if you make sure the 
toc
%Answer
% it =
%      1
% Pn =
%      0     0     0
% Q =
%     0.0963    0.0963    0.0963
% Elapsed time is 0.009856 seconds.
%% Question 4: Starting values: [3,2,1]
tic

a=3;
b=2;
c=1;
it=1;

P=[a,b,c];
Pn = [0,0,0];
while abs(P-Pn)>tol
P=Pn;
Q=exp(-1-P)/(1+exp(-1-P(1))+exp(-1-P(2))+exp(-1-P(3)));
Pn=1./(1.-Q);
it = it+1;
if it>=10000
    break
end  
end
it
Pn
Q
toc
%Answer
% it =
%     10
% Pn =
%     1.0985    1.0985    1.0985
% Q =
%     0.0897    0.0897    0.0897
% Elapsed time is 0.010691 seconds.
%% It does converge for all starting values. However, it is slower than the Broyden's method.
%%

% question #5?