%1
x=[1,1.5,3,4,5,7,9,10];
y1=-2 + .5*x;
y2=-2+.5*x.^2;
Y=[y1;y2];
plot(x,Y);


%2
X=linspace(-10,20,200);
sum(X)


%3
A=[2,4,6;1,7,5;2,12,4];
B=[-2;3;10];
C=A'*B
% inv is slow, use \ like in answer key
D=inv(A'*A)*B
E = sum(sum(A.*B))
F=[A(1,1),A(1,2);A(3,1),A(3,2)]
x=linsolve(A,B)

%4
out=blkdiag(A,A,A,A,A)


%5
% faster way to do A = A>10
A=10+5.*randn(5,3)
A(A<10)=0
A(A>=10)=1


%6
D=csvread('datahw1.csv');
st=num2cell(D,1);
firmid=st{1};
year=st{2};
export=st{3};
rd=st{4};
prod=st{5};
cap=st{6};
const=ones(4380,1);
X=[const,export,rd,cap];
Y=prod;
n=length(Y);
k=4;
B=(X'*X)\X'*Y
e=Y-X*B;
s2=e'*e/(n-k);
se=sqrt(s2*diag(inv(X'*X)))
t=B./se
p=2*(1-tcdf(abs(t),length(Y)-k))
